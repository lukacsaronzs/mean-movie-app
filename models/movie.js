var mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

var movieSchema = new mongoose.Schema({
    title: {type: String},
    description: {type: String},
    release_year: {type: Number},
    price: {type: Number},
    created_by: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
    _created: {type: Date},
    _modified: {type: Date},
    updated_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },

});

movieSchema.plugin(mongoosePaginate);

mongoose.model('Movie', movieSchema);
