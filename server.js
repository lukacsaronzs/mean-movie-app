require('dotenv').config();
var express = require("express");
var bodyParser = require("body-parser");
var movieService = require('./service/movieService');
var userService = require('./service/userService')
var passport = require('passport');
var {mongoose} = require("./db");
var {strategy} = require('./strategy');
var cors = require('cors')




var app = express();

app.use(cors())


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); 
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE, OPTIONS");
    next();
  });

app.use(bodyParser.json());
app.use(passport.initialize());
app.use('/api', userService);
app.use('/api/movies', movieService);





app.listen(8000, () => console.log("Server started."));