
const jwt = require('jsonwebtoken');







const authenticateRefreshJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, process.env.APP_SECRET_REFRESH, (err, user) => {
            if (err) {
                return res.status(401).json({
                    "error": "Invalid or expired JWT token."
                });
            }
            req.user = user
            next();
            return ;
        });
    } else {
        return res.status(400).json({
            "error":  "Missing or malformed JWT token."
        });
    }
};




const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, process.env.APP_SECRET, (err, user) => {
            if (err) {
                return res.status(401).json({
                    "error": "Invalid or expired JWT token."
                });
            }

            req.user = user;


            next();
            return ;
        });
    } else {
        return res.status(400).json({
            "error":  "Missing or malformed JWT token."
        });
    }
};


module.exports = {auth : authenticateJWT, refresh_auth: authenticateRefreshJWT}