const mongoose = require("mongoose");




mongoose.connect(process.env.DB_CONNECTION_STRING, (err) => {
    if(!err){
        console.log("MongoDB connection succeeded!")
    }
    else{
        console.log('Error in DB connection: ' + JSON.stringify(err, undefined,2));
    }
});
mongoose.set('useFindAndModify', false);

module.exports = mongoose;