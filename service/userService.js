const express = require('express');
var router = express.Router();
var passport = require('passport');
var mongoose = require('mongoose');
var userSchema = require('../models/user')
var User = mongoose.model('User');
var refresh_auth = require("../middleware/auth").refresh_auth


function validateEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

function validateUser(user){

    if (user.name == "" || !typeof user.email === 'string' || user.name.length < 5){
      return false;
    }
    if (user.email == "" || !typeof user.email === 'string' || user.email.length < 5 || ! validateEmail(user.email)){
      return false;
    }
    if (user.password == "" || !typeof user.password === 'string' || user.password.length < 5){
      return false;
    }
  return true;
}


router.get('/token',refresh_auth, (req, res) => {
  
      User.findById(req.user._id, (err, user) =>{
        if(err){
          return res.status(500).json({
            "error": err
          })
        }
        token = user.generateJwt()
        res.status(200).json({
          "data" : {
            "token": token

          }
        })
      })

});

router.post('/login', (req, res) => {

    if (!req.body.hasOwnProperty("email") ||  !req.body.hasOwnProperty("password")) {
      return res.status(400).json({
        "error" : "Missing data from request body."
      });
  }

    passport.authenticate('local', function(err, user, info){
        var token;
        var refresh_token;
    
        // If Passport throws/catches an error
        if (err) {
          return res.status(500).json(err);
        }
        // If a user is found
        if(user){
          token = user.generateJwt();
          refresh_token = user.generateRefreshJwt();
          return res.status(200).json({
            "data": {
              "token" : token,
              "refresh_token": refresh_token

            }
          });
        } else {
          // If user is not found
          return res.status(401).json({
            "error" : "Invalid login credentials."
          });
        }
      })(req, res);
});


router.post('/register', (req,res) => {

  if (!req.body.hasOwnProperty("email") || !req.body.hasOwnProperty("name") || !req.body.hasOwnProperty("password")) {
    return res.status(400).json({
      "error" : "Missing data from request body."
    });
 }


  User.find({ email: req.body.email }, function(err, arr) {
    if(!err){
      if(arr.length != 0){
          return res.status(400).json({"error": `User with given email address: ${req.body.email} already exists.`})
      }
      if(validateUser({name: req.body.name, email: req.body.email, password: req.body.password})){
        var user = new User();
        user.name = req.body.name;
        user.email = req.body.email;
        user.setPassword(req.body.password);
      
        user.save((err) => {
          if(!err){
              return res.status(201).json({
                "data" : {
                  "_id": user._id,
                  "name": user.name,
                  "email": user.email
                }
              });
              
          }
          else{
            return res.status(500).json({
              "error" : "Could not save new user."
            });
          }
        });
         
      }
      else{
        console.log('Error in validating user:' + JSON.stringify({name: req.body.name, email: req.body.email, password: req.body.password}, undefined,2));
        return res.status(422).json({
          "error" : "Invalid user data."
        });
      }
        
    }
    else{
      console.log('Error in updating movie:' + JSON.stringify(err, undefined,2));
      return res.status(500).json({"error": "Could not save new user."});

    }
  });


 
    
});

module.exports = router;