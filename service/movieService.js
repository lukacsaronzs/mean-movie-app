const express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var ObjectId = require('mongoose').Types.ObjectId;
var auth = require("../middleware/auth").auth

var movieSchema = require('../models/movie')
var userSchema = require('../models/user')

var Movie = mongoose.model('Movie');
var User = mongoose.model('User');

function validateMovie(movie){


    if (movie.title == "" || !typeof movie.title === 'string' ||  movie.title.length < 5 || movie.title.length > 60 ){
        return false;
    }
    if (movie.description == "" || !typeof movie.description === 'string' ||  movie.description.length < 5 || movie.description.length > 100){
        return false;
    }
    

    if (movie.price == "" || !typeof movie.price === 'number' ||  movie.price < 0 ){
        return false;
    }
    if (movie.release_year == "" || !typeof movie.release_year === 'number' ||  movie.release_year < 1900 || movie.release_year >  new Date().getFullYear()){
        return false;
    }


    return true;
};




const getPagination = (page, size) => {
    const limit = size ? size : 3;
    const offset = page ? page * limit : 0;
  
    return { limit, offset };
  };

router.get('/', auth, (req, res) => {

        const { page, size } = req.query;
        
        //if exists a query in the url, then check the values in the query
        if (Object.keys(req.query).length !== 0){
            if(typeof req.query.page != 'undefined' && (isNaN(page) ||  page < 0 )) {
                return res.status(422).json({
                "error": "Page and size must be positive integers.",
              });
            
            }
            if(typeof req.query.size !== 'undefined' && (isNaN(size) ||  size < 0 )) {
                return res.status(422).json({
                    "error": "Page and size must be positive integers.",
                  });
                
            }
        }   
       

        const { limit, offset } = getPagination(page, size);
        Movie.paginate({}, { offset: offset, limit: limit, populate : [{path: 'created_by', select: 'name'}, {path: 'updated_by' ,select: 'name'}]})
        .then((data) => {
           
        return res.status(200).json(
              {"data":{
                        TotalItems: data.totalDocs,
                        Movies: data.docs,
                        TotalPages: data.totalPages,
                        CurrentPage: data.page - 1,
                    }
                });
        })
        .catch((err) => {
            return res.status(500).json({
            "error": err.message || "Some error occurred while retrieving tutorials.",
          });
        });
});

router.get('/:id',  auth, (req,res) => {
        if(!ObjectId.isValid(req.params.id)){
            return res.status(404).json({"error": `No movie with given id: ${req.params.id}.`})
        }
        Movie.findById(req.params.id).
            populate('created_by', 'name').
            populate('updated_by', 'name').
            exec(function (err, doc) {
                if(!err){
                    if(doc == null){
                        return res.status(404).json({"error": `No movie with given id: ${req.params.id}.`})
    
                    }
                    return res.status(200).json({"data": doc});
                }
                else{
                    console.log('Error in retrieving movie:' + JSON.stringify(err, undefined,2));
                    return res.status(500).json({"error": 'Error in retrieving movie.'});
                }
            });

});

router.put('/:id',  auth, (req,res) => {
    if(!ObjectId.isValid(req.params.id)){
        return res.status(404).json({"error": `No movie with given id: ${req.params.id}.`})
    }
    if (!req.body.hasOwnProperty("title") || !req.body.hasOwnProperty("description") || !req.body.hasOwnProperty("release_year") || !req.body.hasOwnProperty("price") ||!req.hasOwnProperty("user")) {
        return res.status(400).json({
          "error" : "Missing data from request body."
        });
    }

    var movie = {
        title: req.body.title,
        description: req.body.description,
        release_year: req.body.release_year,
        price: req.body.price,
        _modified: Date.now(),
        updated_by: req.user
        };
    if (validateMovie(movie)){
        Movie.findByIdAndUpdate(req.params.id, {$set: movie}, {new: true}).
        populate('created_by', 'name').
        populate('updated_by', 'name').
        exec(function (err, doc) {
            if(!err){
                if(doc == null){
                    return res.status(404).json({"error": `No movie with given id: ${req.params.id}.`})

                }
                return  res.status(200).json({"data": doc});
            }
            else{
                console.log('Error in updating movie:' + JSON.stringify(err, undefined,2));
                return res.status(500).json({"error": "Could not update movie."});

            }
        })
    }
    else{
        console.log('Error in validating movie:' + JSON.stringify(movie, undefined,2));
        return res.status(422).json({"error": "Invalid movie data."})

    }
    
});

router.delete('/:id',  auth, (req,res) => {
    if(!ObjectId.isValid(req.params.id)){
        return res.status(404).json({ "error": `No movie with given id: ${req.params.id}.`})
    }
   
    Movie.findByIdAndDelete(req.params.id)
    .populate("created_by", "name")
    .populate("updated_by", "name")
    .exec(function(err, doc){
        if(!err){
            if (doc == null){
                return res.status(404).json({ "error": `No movie with given id: ${req.params.id}.`})
            }
            return res.status(200).json({"data" : doc});
        }
        else{
            console.log('Error in updating movie:' + JSON.stringify(err, undefined,2));
            return res.status(500).json({"error" : "Could not delete movie."});

        }
    })
});

router.post('/',  auth, (req, res) => {

    if (!req.body.hasOwnProperty("title") || !req.body.hasOwnProperty("description") || !req.body.hasOwnProperty("release_year") || !req.body.hasOwnProperty("price") || !req.hasOwnProperty("user")) {
        return res.status(400).json({
          "error" : "Missing data from request body."
        });
    }

    var movie = new Movie({
        title: req.body.title,
        description: req.body.description,
        release_year: req.body.release_year,
        price: req.body.price,
        _created: Date.now(),
        created_by: req.user._id,
        _modified: Date.now(),
        updated_by: req.user._id


    });
    if (validateMovie(movie)){
        movie.save((err, doc) => {
            if(!err){
                Movie.populate(doc, [{path: 'created_by', select: 'name'}, {path: 'updated_by' ,select: 'name'}], (err, populated) => {
                    if (!err){
                        return res.status(201).json({"data": populated});

                    }
                    console.log('Error in populating movie:' + JSON.stringify(err, undefined,2));
                    return res.status(500).json({"error": "Could not save movie!"});
                })
            }
            else{
                console.log('Error in saving movie:' + JSON.stringify(err, undefined,2));
                return res.status(500).json({"error": "Could not save movie!"});
            }
        });
    }
    else{
        console.log('Error in validating movie:' + JSON.stringify(movie, undefined,2));
        return res.status(422).json({error: "Invalid movie data."})

    }
    
});


module.exports = router;