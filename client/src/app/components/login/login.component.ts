import { Component } from '@angular/core';
import { AuthenticationService, TokenPayload } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { environment } from 'environment';

@Component({
  templateUrl: './login.component.html'
})
export class LoginComponent {
  credentials: TokenPayload = {
    email: '',
    password: ''
  };
  errorMessage: boolean = false;

  constructor(private auth: AuthenticationService, private router: Router) {}

  login() {
    this.errorMessage = false;
    this.auth.login(this.credentials).subscribe(() => {
      
      this.router.navigateByUrl("/movies");
      console.log(this.auth.getUserDetails(this.auth.getToken()))
    }, (err) => {
      this.errorMessage = true;
      console.error(err);
    }); 
  }

  validate(): boolean{
    if (this.credentials.email = ""){
      return false;
    }
    if (this.credentials.password = ""){
      return false;
    }
    return true;
  };
}
