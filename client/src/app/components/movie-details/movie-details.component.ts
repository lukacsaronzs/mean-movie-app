import { Component, OnInit } from '@angular/core';
import { MovieService } from 'src/app/services/movie.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';





function validateReleaseYear(c: FormControl, year: Number) {

  return c.value > 1900 && c.value < year ? null : {valid: false}

}


@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {
  currentMovie : any | null = null;
  message = '';

  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthenticationService){ };
    

  ngOnInit(): void {
   
    this.message = '';
    this.getMovie(this.route.snapshot.paramMap.get('id'));
  };

  getMovie(id: any): void {
    this.movieService.get(id)
      .subscribe(
        data => {
          this.currentMovie = data.data;
          console.log(data.data);
        },
        error => {
          console.log(error);
          this.authService.refresh().subscribe(
            error => {
              console.log(error);
            }) 
        });
  };



  updateMovie(): void {
    this.movieService.update(this.currentMovie!._id, this.currentMovie)
      .subscribe(
        response => {
          console.log(response);
          this.message = 'The movie was updated successfully!';
        },
        error => {
          console.log(error);
          this.authService.refresh().subscribe(
            error => {
              console.log(error);
            }) 
        });
  };

  deleteMovie(): void {
    if (this.currentMovie != null){
      this.movieService.delete(this.currentMovie!._id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/movies']);
        },
        error => {
          console.log(error);
          this.authService.refresh().subscribe(
            error => {
              console.log(error);
            }) 
        });
    }
  };
}