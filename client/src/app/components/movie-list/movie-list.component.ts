import { Component, OnInit } from '@angular/core';
import { MovieService } from 'src/app/services/movie.service';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { isDefined } from '@angular/compiler/src/util';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {

  movies: any;
  pageSizes = [3,5,10,25];
  currentMovie: any;
  currentIndex = -1;
  title = '';
  pager = {
    page : 0,
    pageSize:3, 
    totalPages: 0
  }
  userName: String = "";


  constructor(private movieService: MovieService, private authService: AuthenticationService) { };

  ngOnInit(): void {
    this.retrieveMovies();
    this.userName = this.authService.getUserDetails(this.authService.getToken()).name;

  };

  retrieveMovies(): void {
    this.movieService.getAll({page: this.pager.page, size: this.pager.pageSize})
      .subscribe(
        data => {
          this.movies = data.data.Movies;
          this.pager.page = data.data.CurrentPage;
          this.pager.totalPages = data.data.TotalPages;
          console.log(data);
        },
        error => {
          console.log(error);
          this.authService.refresh().subscribe(
            error => {
              console.log(error);
            }) 
        });
  };

  refreshList(): void {
    this.retrieveMovies();
    this.currentMovie = null;
    this.currentIndex = -1;
  };

  setActiveMovie(movie: any, index: any): void {
    this.currentMovie = movie;
    this.currentIndex = index;
  };
  

  previousPage(): void{
    this.pager.page--;
    this.refreshList();
  };
  nextPage(): void{
    this.pager.page++
    this.refreshList();

  };
  onChangeObj(newObj: Event) {
    this.pager.page = 0;
    this.refreshList();
  };
}

