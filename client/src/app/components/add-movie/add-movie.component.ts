import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { MovieService } from 'src/app/services/movie.service';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit {
  currentMovie = {
    title: '',
    description: '',
    price: 0,
    release_year: 1900
  };
  submitted = false;

  constructor(
    private movieService: MovieService,
    private authService: AuthenticationService
    ) { }

  ngOnInit(): void {
  }

  saveMovie(): void {
    const data = {
      title: this.currentMovie.title,
      description: this.currentMovie.description,
      release_year: this.currentMovie.release_year,
      price: this.currentMovie.price
    };

    this.movieService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
          this.authService.refresh().subscribe(
            error => {
              console.log(error);
            }) 
        });
  }

  newMovie(): void {
    this.submitted = false;
    this.currentMovie = {
      title: '',
      description: '',
      price: 0,
      release_year: 1900
    };
  }

}