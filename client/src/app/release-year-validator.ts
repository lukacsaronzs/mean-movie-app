import { Directive } from '@angular/core';
import { Validator, FormControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[ReleaseYearValidateDirective][formControlName],[ReleaseYearValidateDirective][formControl],[ReleaseYearValidateDirective][ngModel]',
    providers: [ { provide: NG_VALIDATORS, useExisting: AppReleaseYearValidateDirective, multi: true }
    ]
})
export class AppReleaseYearValidateDirective implements Validator {
  validate(control: FormControl) : {[key: string]: any} | null {
    if (control.value < 1900 || control.value > new Date().getFullYear()) {
      console.log(control.value);
      return { 'releaseYearInvalid': true }; // return object if the validation is not passed.
    }
    return null; // return null if validation is passed.
  }
}