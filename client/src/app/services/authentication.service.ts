import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {environment} from "environment"


export interface UserDetails {
  _id: string;
  email: string;
  name: string;
  exp: number;
  iat: number;
}

interface TokenResponse {
  token: string | null;
}

export interface TokenPayload {
  email: string;
  password: string;
  name?: string;
}

@Injectable()
export class AuthenticationService {
  private token: String | null = "";
  private refresh_token: String | null = "";

  constructor(private http: HttpClient, private router: Router) {}

  private saveToken(token: string): void {
    localStorage.setItem('mean-token', token);
    this.token = token;
  }
  private saveTokens(token: string, refresh_token: string): void {
    localStorage.setItem('mean-token', token);
    localStorage.setItem('mean-refresh-token', refresh_token);
    console.log("tokens saved")
    this.token = token;
    this.refresh_token = refresh_token;

  }

  public getToken(): String | null {
    if (!this.token) {
      this.token = localStorage.getItem('mean-token');
    }
    return this.token;
  }
  public getRefreshToken(): String | null {
    if (!this.token) {
      this.token = localStorage.getItem('mean-refresh-token');
    }
    return this.token;
  }

  public getUserDetails(token: String): UserDetails {
    let payload;
    if (token) {
      payload = token.split('.')[1];
      payload = window.atob(payload);
      return JSON.parse(payload);
    } else {
      return {
        _id: "",
        email: "",
        name: "",
        exp: 0,
        iat: 0
      };
    }
  }

  public isRefreshValid(): boolean {
    const user = this.getUserDetails(this.getRefreshToken());
    if (user) {
      console.log(user);
      console.log(Date.now());
      return user.exp > Date.now() / 1000;
    } else {
      return false;
    }
  }

  public isLoggedIn(): boolean {
    const user = this.getUserDetails(this.getToken());
    if (user) {
      console.log(user);
      console.log(Date.now());
      return user.exp > Date.now() / 1000;
    } else {
      return false;
    }
  }

  private request(method: 'post'|'get', type: 'login'|'register'|'token', user?: TokenPayload): Observable<any> {
    let base;
    
     
    if (method === 'post') {
      base = this.http.post(`${environment.API_URL}/${type}`, user);
   
    const request = base.pipe(
      map((res: any) => {
        
        if (res.data.token) {
          this.saveTokens(res.data.token, res.data.refresh_token);
        }
        return res.data;
      })
    );

    return request;
  
    } else {
      base = this.http.get(`${environment.API_URL}/${type}`, { headers: { Authorization: `Bearer ${this.getRefreshToken()}` }});
      const request = base.pipe(
        map((res: any) => {
          if (res.data.token) {
            this.saveToken(res.data.token);
          }
          return res.data;
        })
      );
  
      return request;
    }
    

   
  }

  public register(user: TokenPayload): Observable<any> {
    return this.request('post', 'register', user);
  }
  public login(user: TokenPayload): Observable<any> {

    return this.request('post', 'login', user);
  
  }

  public refresh(): Observable<any> {

    return this.request('get', 'token');
  }

  public logout(): void {
    this.token = '';
    this.refresh_token = '';
    window.localStorage.removeItem('mean-token');
    window.localStorage.removeItem('mean-refresh-token');

    this.router.navigateByUrl('/');
  }
}
