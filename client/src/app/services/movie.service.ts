import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import {environment} from "environment"

const baseUrl = `${environment.API_URL}/movies`;

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http: HttpClient, private auth: AuthenticationService) { }

  getHeader(){

    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.auth.getToken()}`
    });
  };
  getRefreshHeader(){

    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.auth.getRefreshToken()}`
    });
  };


  getAll(params?: any): Observable<any> {
    return this.http.get(baseUrl, {headers:this.getHeader(), params: params});
  }

  get(id: String): Observable<any> {
    return this.http.get(`${baseUrl}/${id}`, {headers:this.getHeader()});
  }

  create(data: any): Observable<any> {
    data.payload = {};
    return this.http.post(baseUrl, data, {headers:this.getHeader()});
  }

  update(id : String, data : any): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data, {headers:this.getHeader()});
  }

  delete(id : String): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`, {headers:this.getHeader()});
  }


  
}